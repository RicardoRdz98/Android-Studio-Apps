package com.orbita.innovacion.proyinte;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Clase de inicilizacion y obtencion de datos (POJO) para el manejo de los eventos
 * que tendra la ainstitucion en su día y día.
 *
 * Fecha de creación: 17/02/2018.
 * Versión: 18.2.15
 * Modificaciones:
 * Ninguna.
 */

public class Item_Evento {

    private String nombre;
    private String mensaje;
    private String img;

    public Item_Evento(String nombre, String mensaje, String img) {
        this.nombre = nombre;
        this.mensaje = mensaje;
        this.img = img;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMensaje() {
        return mensaje;
    }

    public Bitmap getImg() {
        return base64ToBitmap(img);
    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

}
