package com.orbita.innovacion.proyinte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Clase de Fragmento encargada de desplegar los datos importantes del alumno dado de
 * alta dentro de la aplicación.
 *
 * Fecha de creación: 17/01/2018.
 * Versión: 18.2.14
 * Modificaciones:
 * Reacomodo de métodos para el limpiado de inserción de datos de los alumnos.
 * Implementación de método para guardar Token para notificaciones personales
 * desde el servidor de Firebase
 */

public class FragmentHome extends Fragment {
    private TextView matricula, nombre, apat, amat;
    private TextView grupo, grado, turno, periodo;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CircleImageView image;
    private String IDGoogle, datos;

    private String name, tun, peri, matri, a_p, a_m, grup, grad, link, tem;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_fragment_home, container, false);

        matricula = (TextView) v.findViewById(R.id.txtMatricula);
        nombre = (TextView) v.findViewById(R.id.txtNombre);
        apat = (TextView) v.findViewById(R.id.txtAP);
        amat = (TextView) v.findViewById(R.id.txtAM);
        grupo = (TextView) v.findViewById(R.id.txtGrupo);
        grado = (TextView) v.findViewById(R.id.txtGrado);
        turno = (TextView) v.findViewById(R.id.txtTurno);
        periodo = (TextView) v.findViewById(R.id.txtPeriodo);
        image = (CircleImageView) v.findViewById(R.id.alumno);

        IDGoogle = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("IDGoogle","null");
        datos = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Nombre","null");

            DocumentReference miDocRef = FirebaseFirestore.getInstance().document("Escuela/" + IDGoogle);

            miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                    if(documentSnapshot.exists()){
                        limpiar();

                        name = documentSnapshot.getString("nombre");
                        tun = documentSnapshot.getString("turno");
                        peri = documentSnapshot.getString("Periodo");
                        matri = documentSnapshot.getString("Matricula");
                        a_p = documentSnapshot.getString("Apellido_Paterno");
                        a_m = documentSnapshot.getString("Apellido_Materno");
                        grup = documentSnapshot.getString("grupo");
                        grad = documentSnapshot.getString("grado");
                        link = documentSnapshot.getString("link");
                        tem = documentSnapshot.getString("tema");

                        llenar();
                    }else{
                        if(datos.toString().equals("")){
                        }else{
                            /*Intent intent = new Intent(getContext(), NoRegistrado.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);*/
                        }
                    }
                }
            });
        token();
        return v;
    }

    private void llenar(){
        matricula.setText(matri);
        nombre.setText(name);
        apat.setText(a_p);
        amat.setText(a_m);
        grupo.setText(grup);
        grado.setText(grad);
        turno.setText(tun);
        periodo.setText(peri);

        Glide.with(this)
                .load(link)
                .into(image);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("Tema");
        editor.putString("Tema", tem);
        editor.apply();
    }

    private void limpiar(){
        matricula.setText(null);
        nombre.setText(null);
        apat.setText(null);
        amat.setText(null);
        grupo.setText(null);
        grado.setText(null);
        turno.setText(null);
        periodo.setText(null);
    }

    private void token(){
        String token = FirebaseInstanceId.getInstance().getToken();
        db.collection("Escuela").document(IDGoogle).update("Token_Notification", token);
    }

}
