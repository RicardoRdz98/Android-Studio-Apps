package com.orbita.innovacion.ejemplo13fragmentos;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.orbita.innovacion.ejemplo13fragmentos.fragmentos.Agregar;
import com.orbita.innovacion.ejemplo13fragmentos.fragmentos.Eliminar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Agregar.CallBackActivity{//, Eliminar.CallBackFragment {

    private Button add, show, delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        add = (Button) findViewById(R.id.btn_add);
        show = (Button) findViewById(R.id.btn_show);
        delete = (Button) findViewById(R.id.btn_delete);

        add.setOnClickListener(this);
        show.setOnClickListener(this);
        delete.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        FragmentManager fm = getFragmentManager(); //No sea la v4
        FragmentTransaction fragmentTransaction = fm.beginTransaction(); //Comenzamos una transaccion del fragmento
        switch (v.getId()){
            case R.id.btn_add:
                fragmentTransaction.replace(R.id.container, new Agregar()).commit();
                add.setEnabled(false);
                show.setEnabled(false);
                delete.setEnabled(false);
                break;
        }
    }

    @Override
    public void getActivity(String nombre, String lastName, String password) {
        Toast.makeText(this, "Fragment - Activity", Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Name: " + nombre, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Last Name: " + lastName, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Password: " + password, Toast.LENGTH_SHORT).show();

        add.setEnabled(true);
        show.setEnabled(true);
        delete.setEnabled(true);
    }

    /*@Override
    public void getFragment(String nombre, String lastName, String password) {
        Agregar agregar = (Agregar) getFragmentManager().findFragmentById(R.id.FragmentAgregar);
        agregar.recivir(nombre, lastName, password);
    }*/
}
