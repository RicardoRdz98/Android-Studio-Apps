package com.orbita.innovacion.ejemplo13fragmentos.fragmentos;

import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.orbita.innovacion.ejemplo13fragmentos.R;

/**
 * Created by ricardo on 14/03/18.
 */

public class Eliminar extends Fragment {

    private EditText name, lastname, password;
    private Button button;
    private Eliminar.CallBackFragment callBackFragment;

    //Este metodo se ejecuta automaticamente cuando se ejecua el constrictor
    // es importante porque aqui inflamos en layout y manipulamos los widgets
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_eliminar, container, false);

        name = (EditText) v.findViewById(R.id.edt_nombre);
        lastname = (EditText) v.findViewById(R.id.edt_apellido);
        password = (EditText) v.findViewById(R.id.edt_password);
        button = (Button) v.findViewById(R.id.btn_Aceptar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Segundo Fragment", Toast.LENGTH_SHORT).show();
                getActivity()
                        .getFragmentManager()
                        .beginTransaction()
                        .remove(Eliminar.this)
                        .commit();

                /*Agregar agregar = new Agregar();
                Bundle args = new Bundle();
                args.putString("name", name.getText().toString());
                args.putString("lastname", lastname.getText().toString());
                args.putString("password", password.getText().toString());
                agregar.setArguments(args);*/

                String nom = name.getText().toString();
                String ape = lastname.getText().toString();
                String pas = password.getText().toString();

                callBackFragment.getFragment(nom, ape, pas);
                //Toast.makeText(getActivity(), name.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    //Se ejecuta cuando cargamos el layout
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public interface CallBackFragment{
        public void getFragment(String nombre, String lastName, String password);
    }

    //Genera el procso de CallBack el cual es impresindiblle si queremos regresarun valor
    // a la actividad que lo esta llamando
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            //UpCasting
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                callBackFragment = (CallBackFragment)getContext();
            }else{
                callBackFragment = (CallBackFragment)getActivity();
            }
        }catch (ClassCastException e){
            e.printStackTrace();
        }
    }
}
