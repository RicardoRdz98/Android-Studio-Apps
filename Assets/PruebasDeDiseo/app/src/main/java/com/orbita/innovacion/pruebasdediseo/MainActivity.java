package com.orbita.innovacion.pruebasdediseo;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText uno, dos;
    Button ok;
    String one, two;

    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uno = (EditText) findViewById(R.id.uno);
        dos = (EditText) findViewById(R.id.dos);
        ok = (Button) findViewById(R.id.button2);
        linearLayout = (LinearLayout) findViewById(R.id.myLayout);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one = uno.getText().toString();
                two = dos.getText().toString();

                Snackbar.make(linearLayout, "Uno: " + one + ", " + " Dos: " + two, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //Toast.makeText(MainActivity.this, "Uno: " + one + ", " + " Dos: " + two, Toast.LENGTH_SHORT).show();

            }
        });

    }
}