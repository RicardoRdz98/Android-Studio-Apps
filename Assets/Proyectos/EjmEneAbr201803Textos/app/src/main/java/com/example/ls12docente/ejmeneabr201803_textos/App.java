package com.example.ls12docente.ejmeneabr201803_textos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class App extends AppCompatActivity implements View.OnClickListener,TextView.OnEditorActionListener{
    private EditText name=null,edad=null,comment=null;
    private Button btnAcept = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app);
        name     = (EditText)findViewById(R.id.edtName);
        edad     = (EditText)findViewById(R.id.EdtEdad);
        comment  = (EditText)findViewById(R.id.edtMulti);
        btnAcept = (Button)findViewById(R.id.btnAceptar);
        btnAcept.setOnClickListener(this);

     //   name.setOnEditorActionListener(this);
      //  edad.setOnEditorActionListener(this);
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnAcept.setEnabled (!name.getText().toString().isEmpty()
                        && !edad.getText().toString().isEmpty());
            }
        });
        edad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnAcept.setEnabled (!name.getText().toString().isEmpty()
                        && !edad.getText().toString().isEmpty());
            }
        });

        btnAcept.setEnabled(false);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.edtName){
            name.setText("");
            Toast.makeText(this, "Ok soy el nombre", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

        if (!keyEvent.isShiftPressed()){
            btnAcept.setEnabled (!name.getText().toString().isEmpty()
                    && !edad.getText().toString().isEmpty());

            Toast.makeText(this, textView.getText().toString(), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}








