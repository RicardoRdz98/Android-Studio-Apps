package com.example.ls12docente.ejm004ene_abr2018spinner;

import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class App extends AppCompatActivity {
    private Spinner                   spn02=null;
    private mySpinner mySpinner            = null;
    private ArrayList<mySpinner>      list =null; //guarda cuantos objetos voy a poner
    private ArrayAdapter<mySpinner> adapter=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app);
        fillSpinner();
    }
    private void fillSpinner(){
        list =  new ArrayList<>();
        list.add(new mySpinner(0,"Selecciona..."));
        for (int i=1;i<=5;i++){
            list.add(new mySpinner(i,"Ejemplo "+i));
        }
        spn02   = (Spinner)findViewById(R.id.spn02); //tenemos el objeto spiner para trabajar
       // adapter = new ArrayAdapter<mySpinner>(this, android.R.layout.simple_spinner_dropdown_item,list);
        adapter = new ArrayAdapter<mySpinner>(this, R.layout.myspinner_layout,list);
        spn02.setAdapter(adapter);
        spn02.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                 Object papa = (mySpinner)adapterView.getItemAtPosition(pos);
                 int     id   = ((mySpinner)papa).getId();
                String   name =((mySpinner)papa).getName();
                Toast.makeText(App.this, name, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
}













