package com.orbita.innovacion.firebase_appprueba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class Firestore extends AppCompatActivity {

    TextView accion, mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firestore);

        accion = (TextView) findViewById(R.id.accion);
        mensaje = (TextView) findViewById(R.id.mensaje);

        DocumentReference miDocRef = FirebaseFirestore.getInstance().document("PruebaDatos/yvo2gNn4dCvdrmHroA4d");

        miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){

                    accion.setText(documentSnapshot.getString("accion"));
                    mensaje.setText(documentSnapshot.getString("mensaje"));

                }else{
                }
            }
        });

    }
}
