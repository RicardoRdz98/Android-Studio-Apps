package com.orbita.innovacion.ejemplo09hilos;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class App extends AppCompatActivity implements View.OnClickListener{

    private TextView text;
    private Button start, stop;
    private short seg = 0, min = 0, hor = 0;
    private Cronometro crono;
    private boolean flag = true;
    LinearLayout miLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app);
        start = (Button) findViewById(R.id.play);
        stop = (Button) findViewById(R.id.stop);
        text = (TextView) findViewById(R.id.texto);
        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        stop.setEnabled(false);
        crono = new Cronometro();
        miLayout = (LinearLayout) findViewById(R.id.main);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.play){
            if(crono.getStatus() != AsyncTask.Status.RUNNING){ //si no esta corriendo, comienza a correr
                flag = true;
                crono = new Cronometro();
                start.setText("PAUSE");
                stop.setEnabled(true);
                crono.execute(seg, min, hor);
            }else{
                //si si esta corriendo killem all threads
                crono.cancel(true);
                start.setText("PLAY");
                stop.setEnabled(false);
                flag = false;
            }
        }

        if(v.getId() == R.id.stop){
            crono.cancel(true);
            stop.setEnabled(false);
            start.setText("PLAY");
            hor = min = seg = 0;
        }
    }

    class Cronometro extends AsyncTask<Short, Short, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            text.setText("00:00:00");
            //hor = min = seg = 0;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);

            Snackbar.make(miLayout, "Se detuvo la cuenta", Snackbar.LENGTH_LONG).show();
        }

        @Override
        protected String doInBackground(Short... strings) {
            /*short seg = strings[0];
            short min = strings[1];
            short hor = strings[2];*/
            try {
                while (flag){
                    seg++;
                    if(seg == 60){
                        seg = 0;
                        min++;
                    }
                    if(min == 60){
                        min = 0;
                        hor++;
                    }
                    publishProgress(seg, min, hor); // TA MAL ALV
                    Thread.sleep(1000);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Short... values) {
            super.onProgressUpdate(values);
            String ss = (values[0] < 10)?"0" + values[0]:"" + values[0];
            String mm = (values[1] < 10)?"0" + values[1]:"" + values[1];
            String hh = (values[2] < 10)?"0" + values[2]:"" + values[2];
            text.setText(""+hh+":"+mm+":"+ss);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

}
