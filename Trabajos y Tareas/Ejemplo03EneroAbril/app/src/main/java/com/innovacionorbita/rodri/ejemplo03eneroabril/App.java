package com.innovacionorbita.rodri.ejemplo03eneroabril;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class App extends AppCompatActivity implements View.OnClickListener, TextView.OnEditorActionListener{

    private EditText nombre, edad, comment;
    private Button btnAcept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        nombre =(EditText) findViewById(R.id.edtname);
        edad = (EditText) findViewById(R.id.edtedad);
        comment = (EditText) findViewById(R.id.edtmulti);
        btnAcept = (Button) findViewById(R.id.btmacept);
        btnAcept.setOnClickListener(this);

        btnAcept.setEnabled(false);

        nombre.setOnClickListener(this);

        nombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                btnAcept.setEnabled(!nombre.getText().toString().isEmpty() && edad.getText().toString().isEmpty());
                //Toast.makeText(this, s.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.edtname){
            nombre.setText("");
            Toast.makeText(this, "EdiText OK", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        if(!event.isShiftPressed()){
            btnAcept.setEnabled(!nombre.getText().toString().isEmpty() && edad.getText().toString().isEmpty());
            Toast.makeText(this, v.getText().toString(), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}
