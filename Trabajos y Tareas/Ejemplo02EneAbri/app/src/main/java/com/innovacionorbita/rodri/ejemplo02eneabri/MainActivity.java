package com.innovacionorbita.rodri.ejemplo02eneabri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btmaceptar, btmcancelar, btmcerrar;
    private EditText edtname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btmcancelar = (Button) findViewById(R.id.btm_Cancelar);
        btmcerrar = (Button) findViewById(R.id.btm_cerrar);
        btmcerrar.setOnClickListener(this);

        btmcancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Boton Cancelar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void Aceptacion(View view) {
        Toast.makeText(this, "Boton Aceptar", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btm_cerrar){
            Toast.makeText(this, "Boton Cerrar", Toast.LENGTH_SHORT).show();
        }
    }
}
