package com.orbita.innovacion.ejemplo06listview;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<ListItem> items = null;
    private ListView lista = null;
    private AdapterListItem adapter = null;
    private final int cuantos = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista =  (ListView) findViewById(R.id.List);
        items = new ArrayList<>();

        for (int i = 0; i<cuantos; i++){
            items.add(new ListItem("Titulo: " + i, " Subtitulo: " + i));
        }

        adapter = new AdapterListItem(this, items);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

    }

    class AdapterListItem extends ArrayAdapter<ListItem>{

        public AdapterListItem(@NonNull Context context, int resource) {
            super(context, resource);
        }
        Context context;
        public AdapterListItem(Context context, ArrayList<ListItem> items){
            super(context, 0, items);
            this.context = context;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //return super.getView(position, convertView, parent);
            View item = convertView;
            ListItem user = getItem(position);
            TextView titulo = null, subtitulo = null;
            if (item == null){
                item = LayoutInflater.from(getContext()).inflate(R.layout.item, parent, false);
            }
            TextView txtname = (TextView) item.findViewById(R.id.titulo);
            TextView txtsubtitule = (TextView) item.findViewById(R.id.suptitulo);

            txtname.setText(user.getTitulo());
            txtsubtitule.setText(user.getSubtitulo());
            return item;
        }
    }

}
